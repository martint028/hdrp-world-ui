# README #

This sample project shows how to get world-space UI elements working more like you expect with two simple shaders.

I wrote a blog about everything here: https://gamasutra.com/blogs/TravisHawks/20200224/358618/Fixing_WorldSpace_UI_with_Unitys_HDRP.php

### Version Info ###

* This was created using Unity 2019.3.0f3 and HDRP 7.1.6
* These fixes should work in future versions of Unity and HDRP, but use on earlier versions is not guaranteed.

### How to Use ###

* Clone the project locally and take a look at the Shader Graphs and materials created.
* A Unity Package has also been included that can be easily exported to your project if you wish to get up and running or investigate the materials and shaders in your own project.
* Apply the materials to your world-space Image and Text UI elements to have the UI appear as you expect.

### Questions? ###

* Feel free to contact me with any questions, or leave a comment on the blog post to help others.
